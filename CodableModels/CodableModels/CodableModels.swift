//
//  CodableModels.swift
//  CodableModels
//
//  Created by Eduard Churikov on 17.01.2022.
//

import Foundation

public struct MarvelHeroesResponse: Codable {
  public let data: MarvelHeroesData?
}

public struct MarvelHeroesData: Codable {
  public let results: [ModelHeroCell]
}

public struct ModelHeroCell: Codable {
  public let name: String?
  public let thumbnail: Thumbnail?
}

public struct Thumbnail: Codable {
  public let path: String
  public let thumbnailExtension: Extension

  public enum CodingKeys: String, CodingKey {
    case path
    case thumbnailExtension = "extension"
  }
}

public enum Extension: String, Codable {
  case gif
  case jpg
}



