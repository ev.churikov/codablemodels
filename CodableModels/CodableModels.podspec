

Pod::Spec.new do |spec|



  spec.name         = "CodableModels"
  spec.version      = "0.0.1"
  spec.summary      = "Short description of CodableModels."

  spec.description  = <<-DESC
  CodableModels - содержит все модели участвующие в десериализации данных из сети 
                   DESC

   spec.homepage     = "https://gitlab.com/ev.churikov/codablemodels.git"


  spec.license      = { :type => "MIT", :file => "LICENSE" }


  spec.author       = { "Eduard Churikov" => "fillatd8@gmail.com" }

  spec.source       = { :git => "https://gitlab.com/ev.churikov/codablemodels", :tag=>
  "#{spec.version}" }


  spec.source_files  = "CodableModels/**/*.{h,m,swift,xib,storyboard}"

  spec.exclude_files = "Classes/Exclude"

  spec.ios.deployment_target = "12.1"

  spec.swift_version         = "5.0"

  spec.platform = :ios, "14.4" 


end



